/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 66915CB9
/// @DnDArgument : "expr" "room"
var l66915CB9_0 = room;
switch(l66915CB9_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 77A2629B
	/// @DnDParent : 66915CB9
	/// @DnDArgument : "const" "rm_Game"
	case rm_Game:
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Lives
		/// @DnDVersion : 1
		/// @DnDHash : 203A8CF6
		/// @DnDParent : 77A2629B
		/// @DnDArgument : "x" "x"
		/// @DnDArgument : "x_relative" "1"
		/// @DnDArgument : "y" "y"
		/// @DnDArgument : "y_relative" "1"
		/// @DnDArgument : "sprite" "spr_Lives"
		/// @DnDSaveInfo : "sprite" "spr_Lives"
		var l203A8CF6_0 = sprite_get_width(spr_Lives);
		var l203A8CF6_1 = 0;
		if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
		for(var l203A8CF6_2 = __dnd_lives; l203A8CF6_2 > 0; --l203A8CF6_2) {
			draw_sprite(spr_Lives, 0, x + x + l203A8CF6_1, y + y);
			l203A8CF6_1 += l203A8CF6_0;
		}
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 572628C0
	/// @DnDParent : 66915CB9
	/// @DnDArgument : "const" "rm_Menu"
	case rm_Menu:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 67DFBD09
		/// @DnDParent : 572628C0
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 43894C93
		/// @DnDParent : 572628C0
		/// @DnDArgument : "color" "$FF0F0FFF"
		/// @DnDArgument : "alpha" "false"
		draw_set_colour($FF0F0FFF & $ffffff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 447E07B5
		/// @DnDParent : 572628C0
		/// @DnDArgument : "x" "400"
		/// @DnDArgument : "y" "200"
		/// @DnDArgument : "xscale" "3"
		/// @DnDArgument : "yscale" "3"
		/// @DnDArgument : "caption" ""THE VIRUS AMONGST US""
		draw_text_transformed(400, 200, string("THE VIRUS AMONGST US") + "", 3, 3, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 351D7EC8
		/// @DnDParent : 572628C0
		/// @DnDArgument : "alpha" "false"
		draw_set_colour($FFFFFFFF & $ffffff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 6FB6EBE5
		/// @DnDParent : 572628C0
		/// @DnDArgument : "x" "400"
		/// @DnDArgument : "y" "280"
		/// @DnDArgument : "caption" ""Make it to the other side... alive!""
		draw_text(400, 280, string("Make it to the other side... alive!") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 57EEF2E5
		/// @DnDParent : 572628C0
		/// @DnDArgument : "x" "400"
		/// @DnDArgument : "y" "330"
		/// @DnDArgument : "caption" ""Use A and D to move""
		draw_text(400, 330, string("Use A and D to move") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 6E96FFFD
		/// @DnDParent : 572628C0
		/// @DnDArgument : "x" "400"
		/// @DnDArgument : "y" "370"
		/// @DnDArgument : "caption" ""Press Space to jump""
		draw_text(400, 370, string("Press Space to jump") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 2AE30B1C
		/// @DnDParent : 572628C0
		/// @DnDArgument : "x" "400"
		/// @DnDArgument : "y" "410"
		/// @DnDArgument : "caption" ""Press O for more options""
		draw_text(400, 410, string("Press O for more options") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 430950FC
		/// @DnDParent : 572628C0
		/// @DnDArgument : "color" "$FF0F0FFF"
		/// @DnDArgument : "alpha" "false"
		draw_set_colour($FF0F0FFF & $ffffff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 775341EA
		/// @DnDParent : 572628C0
		/// @DnDArgument : "x" "400"
		/// @DnDArgument : "y" "460"
		/// @DnDArgument : "caption" ""PRESS ENTER TO START...""
		draw_text(400, 460, string("PRESS ENTER TO START...") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 5BF8C52A
		/// @DnDParent : 572628C0
		/// @DnDArgument : "alpha" "false"
		draw_set_colour($FFFFFFFF & $ffffff);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 2FAB4B4E
		/// @DnDParent : 572628C0
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 053062AC
	/// @DnDParent : 66915CB9
	/// @DnDArgument : "const" "rm_Defeat"
	case rm_Defeat:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 489F5B42
		/// @DnDParent : 053062AC
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 31DEE07E
		/// @DnDParent : 053062AC
		/// @DnDArgument : "color" "$FF282DFF"
		/// @DnDArgument : "alpha" "false"
		draw_set_colour($FF282DFF & $ffffff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 2BB93A83
		/// @DnDParent : 053062AC
		/// @DnDArgument : "x" "400"
		/// @DnDArgument : "y" "200"
		/// @DnDArgument : "xscale" "2"
		/// @DnDArgument : "yscale" "2"
		/// @DnDArgument : "caption" ""Little did you know this day would be your last...""
		draw_text_transformed(400, 200, string("Little did you know this day would be your last...") + "", 2, 2, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 671A7B7C
		/// @DnDParent : 053062AC
		/// @DnDArgument : "alpha" "false"
		draw_set_colour($FFFFFFFF & $ffffff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 62C9D3B2
		/// @DnDParent : 053062AC
		/// @DnDArgument : "x" "400"
		/// @DnDArgument : "y" "300"
		/// @DnDArgument : "caption" ""PRESS ENTER TO TRY AGAIN... ""
		draw_text(400, 300, string("PRESS ENTER TO TRY AGAIN... ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 6E483D96
		/// @DnDParent : 053062AC
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 276CB69D
	/// @DnDParent : 66915CB9
	/// @DnDArgument : "const" "rm_Victory"
	case rm_Victory:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 58C92BF2
		/// @DnDParent : 276CB69D
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 4C93AAC5
		/// @DnDParent : 276CB69D
		/// @DnDArgument : "color" "$FF21FF21"
		/// @DnDArgument : "alpha" "false"
		draw_set_colour($FF21FF21 & $ffffff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 09ABB8EC
		/// @DnDParent : 276CB69D
		/// @DnDArgument : "x" "400"
		/// @DnDArgument : "y" "200"
		/// @DnDArgument : "xscale" "2"
		/// @DnDArgument : "yscale" "2"
		/// @DnDArgument : "caption" ""You made it safely through another day!""
		draw_text_transformed(400, 200, string("You made it safely through another day!") + "", 2, 2, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 003B5399
		/// @DnDParent : 276CB69D
		/// @DnDArgument : "alpha" "false"
		draw_set_colour($FFFFFFFF & $ffffff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 29684A7C
		/// @DnDParent : 276CB69D
		/// @DnDArgument : "x" "400"
		/// @DnDArgument : "y" "300"
		/// @DnDArgument : "caption" ""PRESS ENTER TO PLAY AGAIN""
		draw_text(400, 300, string("PRESS ENTER TO PLAY AGAIN") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 6FD9903E
		/// @DnDParent : 276CB69D
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;
}