/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 3D1C810D
/// @DnDArgument : "expr" "room"
var l3D1C810D_0 = room;
switch(l3D1C810D_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 770336AF
	/// @DnDParent : 3D1C810D
	/// @DnDArgument : "const" "rm_Menu"
	case rm_Menu:
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 21943611
		/// @DnDParent : 770336AF
		/// @DnDArgument : "room" "rm_Game"
		/// @DnDSaveInfo : "room" "rm_Game"
		room_goto(rm_Game);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 08AC95D5
	/// @DnDParent : 3D1C810D
	/// @DnDArgument : "const" "rm_Victory"
	case rm_Victory:
		/// @DnDAction : YoYo Games.Game.Restart_Game
		/// @DnDVersion : 1
		/// @DnDHash : 6C9CE22B
		/// @DnDParent : 08AC95D5
		game_restart();
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 6613982B
	/// @DnDParent : 3D1C810D
	/// @DnDArgument : "const" "rm_Defeat"
	case rm_Defeat:
		/// @DnDAction : YoYo Games.Game.Restart_Game
		/// @DnDVersion : 1
		/// @DnDHash : 67F82C32
		/// @DnDParent : 6613982B
		game_restart();
		break;
}