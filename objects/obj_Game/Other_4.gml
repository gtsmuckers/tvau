/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3A24C0E0
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "rm_Menu"
if(room == rm_Menu)
{
	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 2D23E8D0
	/// @DnDParent : 3A24C0E0
	/// @DnDArgument : "soundid" "snd_MenuMusic"
	/// @DnDArgument : "loop" "1"
	/// @DnDSaveInfo : "soundid" "snd_MenuMusic"
	audio_play_sound(snd_MenuMusic, 0, 1);
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1F634E78
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "rm_Game"
if(room == rm_Game)
{
	/// @DnDAction : YoYo Games.Audio.If_Audio_Playing
	/// @DnDVersion : 1
	/// @DnDHash : 2CEC90F0
	/// @DnDParent : 1F634E78
	/// @DnDArgument : "soundid" "snd_MenuMusic"
	/// @DnDSaveInfo : "soundid" "snd_MenuMusic"
	var l2CEC90F0_0 = snd_MenuMusic;
	if (audio_is_playing(l2CEC90F0_0))
	{
		/// @DnDAction : YoYo Games.Audio.Stop_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 4BD7A5B3
		/// @DnDParent : 2CEC90F0
		/// @DnDArgument : "soundid" "snd_MenuMusic"
		/// @DnDSaveInfo : "soundid" "snd_MenuMusic"
		audio_stop_sound(snd_MenuMusic);
	}

	/// @DnDAction : YoYo Games.Audio.If_Audio_Playing
	/// @DnDVersion : 1
	/// @DnDHash : 12547340
	/// @DnDParent : 1F634E78
	/// @DnDArgument : "soundid" "snd_GameMusic"
	/// @DnDSaveInfo : "soundid" "snd_GameMusic"
	var l12547340_0 = snd_GameMusic;
	if (audio_is_playing(l12547340_0))
	{
		/// @DnDAction : YoYo Games.Audio.Stop_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 7345BFED
		/// @DnDParent : 12547340
		/// @DnDArgument : "soundid" "snd_GameMusic"
		/// @DnDSaveInfo : "soundid" "snd_GameMusic"
		audio_stop_sound(snd_GameMusic);
	}

	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 0F947375
	/// @DnDParent : 1F634E78
	/// @DnDArgument : "soundid" "snd_GameMusic"
	/// @DnDArgument : "loop" "1"
	/// @DnDSaveInfo : "soundid" "snd_GameMusic"
	audio_play_sound(snd_GameMusic, 0, 1);
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4B4AD877
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "rm_Victory"
if(room == rm_Victory)
{
	/// @DnDAction : YoYo Games.Audio.If_Audio_Playing
	/// @DnDVersion : 1
	/// @DnDHash : 797A15FF
	/// @DnDParent : 4B4AD877
	/// @DnDArgument : "soundid" "snd_GameMusic"
	/// @DnDSaveInfo : "soundid" "snd_GameMusic"
	var l797A15FF_0 = snd_GameMusic;
	if (audio_is_playing(l797A15FF_0))
	{
		/// @DnDAction : YoYo Games.Audio.Stop_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 1C252399
		/// @DnDParent : 797A15FF
		/// @DnDArgument : "soundid" "snd_GameMusic"
		/// @DnDSaveInfo : "soundid" "snd_GameMusic"
		audio_stop_sound(snd_GameMusic);
	}

	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 046D58D6
	/// @DnDParent : 4B4AD877
	/// @DnDArgument : "soundid" "snd_Victory"
	/// @DnDSaveInfo : "soundid" "snd_Victory"
	audio_play_sound(snd_Victory, 0, 0);
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 6DC7D4B6
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "rm_Defeat"
if(room == rm_Defeat)
{
	/// @DnDAction : YoYo Games.Audio.If_Audio_Playing
	/// @DnDVersion : 1
	/// @DnDHash : 3C993228
	/// @DnDParent : 6DC7D4B6
	/// @DnDArgument : "soundid" "snd_GameMusic"
	/// @DnDSaveInfo : "soundid" "snd_GameMusic"
	var l3C993228_0 = snd_GameMusic;
	if (audio_is_playing(l3C993228_0))
	{
		/// @DnDAction : YoYo Games.Audio.Stop_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 2E2F05D6
		/// @DnDParent : 3C993228
		/// @DnDArgument : "soundid" "snd_GameMusic"
		/// @DnDSaveInfo : "soundid" "snd_GameMusic"
		audio_stop_sound(snd_GameMusic);
	}

	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 259478ED
	/// @DnDParent : 6DC7D4B6
	/// @DnDArgument : "soundid" "snd_Defeat"
	/// @DnDSaveInfo : "soundid" "snd_Defeat"
	audio_play_sound(snd_Defeat, 0, 0);
}