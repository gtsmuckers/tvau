/// @DnDAction : YoYo Games.Audio.If_Audio_Playing
/// @DnDVersion : 1
/// @DnDHash : 727776D1
/// @DnDArgument : "soundid" "snd_PlayerHurt"
/// @DnDSaveInfo : "soundid" "snd_PlayerHurt"
var l727776D1_0 = snd_PlayerHurt;
if (audio_is_playing(l727776D1_0))
{
	/// @DnDAction : YoYo Games.Audio.Stop_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 6D2DDDAD
	/// @DnDParent : 727776D1
	/// @DnDArgument : "soundid" "snd_PlayerHurt"
	/// @DnDSaveInfo : "soundid" "snd_PlayerHurt"
	audio_stop_sound(snd_PlayerHurt);
}

/// @DnDAction : YoYo Games.Audio.Play_Audio
/// @DnDVersion : 1
/// @DnDHash : 1A8E82A9
/// @DnDArgument : "soundid" "snd_PlayerHurt"
/// @DnDSaveInfo : "soundid" "snd_PlayerHurt"
audio_play_sound(snd_PlayerHurt, 0, 0);