/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1894CAB6
/// @DnDArgument : "var" "bbox_bottom"
/// @DnDArgument : "op" "2"
/// @DnDArgument : "value" "room_height"
if(bbox_bottom > room_height)
{
	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 1B0D3F7F
	/// @DnDParent : 1894CAB6
	/// @DnDArgument : "soundid" "snd_Falling"
	/// @DnDSaveInfo : "soundid" "snd_Falling"
	audio_play_sound(snd_Falling, 0, 0);

	/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
	/// @DnDVersion : 1
	/// @DnDHash : 13688E10
	/// @DnDParent : 1894CAB6
	/// @DnDArgument : "lives" "-1"
	/// @DnDArgument : "lives_relative" "1"
	if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
	__dnd_lives += real(-1);

	/// @DnDAction : YoYo Games.Movement.Jump_To_Point
	/// @DnDVersion : 1
	/// @DnDHash : 10720A84
	/// @DnDParent : 1894CAB6
	/// @DnDArgument : "x" "178"
	/// @DnDArgument : "y" "605"
	x = 178;
	y = 605;
}