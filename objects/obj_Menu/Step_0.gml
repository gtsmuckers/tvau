//get input
var _up = keyboard_check_pressed(ord("W"));
var _down = keyboard_check_pressed(ord("S"));
var _select = keyboard_check_pressed(vk_enter) or keyboard_check_pressed(vk_space);

var _move = _down - _up;
if _move != 0 {
	//move the index
	index += _move;
}
	//clamp values
	var _size = array_length(menu);
	if index < 0 index = _size - 1;		//at start, loop to menu end
	else if index >= _size index = 0;	//at end, loop to menu start